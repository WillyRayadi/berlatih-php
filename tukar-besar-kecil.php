<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php 
	echo "<h3>Soal No 3 Tukar Besar Kecil </h3>";
	//echo "<br>";

	// Buatlah sebuah file dengan nama tukar-besar-kecil.php. Di dalam file tersebut buatlah function dengan nama tukar_besar_kecil yang menerima parameter berupa string. function akan mengembalikan sebuah string yang sudah ditukar ukuran besar dan kecil per karakter yang ada di parameter. Contohnya jika parameter “pHp” maka akan mengembalikan “PhP”.

	function tukar_besar_kecil($string){
		
		for($i=0; $i < strlen($string); $i++){
			if(ctype_lower($string[$i])) {
				$new_string[$i] = strtoupper($string[$i]);
			}else{
				$new_string[$i] = strtolower($string[$i]);
			}
		}
		return implode($new_string). "<br>";
	}

	// TEST CASES
	echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
	echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
	echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
	echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
	echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

	/*$strings = array('AKLWC139', 'LMNSDO', 'akwSKWsm');
	foreach ($strings as $testcase) {
    	if (ctype_upper($testcase)) {
        echo "The string $testcase consists of all uppercase letters.\n";
	    } else {
	        echo "The string $testcase does not consist of all uppercase letters.\n";
	    }
	}*/
	?>
</body>
</html>