<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
	echo "<h3>Soal No 2 Ubah Huruf </h3>";
	//echo "<br>";

	// Buatlah sebuah file dengan nama ubah-huruf.php. Di dalam file tersebut buatlah sebuah function dengan nama ubah_huruf yang menerima parameter berupa string. function akan mereturn string yang berisi karakter-karakter yang sudah diubah dengan karakter setelahnya dalam susunan abjad “abdcde …. xyz”. Contohnya karakter “a” akan diubah menjadi “b” karakter “x” akan berubah menjadi “y”, dst.

	function ubah_huruf($string){
		
		$abjad = "abcdefghijklmnopqrstuvwxyz";
		$output = "";
		for($i=0; $i < strlen($string); $i++){
			$posisi = stripos($abjad, $string[$i]);
			$output .= substr($abjad, $posisi + 1, 1);
		}
		return $output."<br>";
		/*$tampung = $string;
		if($tampung == "wow"){
			$tampung = "xpx";
		}else if($tampung == "developer"){
			$tampung = "efwfmpqfs";
		}else if ($tampung == "laravel") {
			$tampung = "mbsbwfm";
		}else if ($tampung == "keren") {
			$tampung = "lfsfo";
		}else if ($tampung == "semangat") {
			$tampung = "tfnbohbu";
		}
		return $tampung);*/
	}

	// TEST CASES
	echo ubah_huruf('wow'); // xpx
	echo "<br>";
	echo ubah_huruf('developer'); // efwfmpqfs
	echo "<br>";
	echo ubah_huruf('laravel'); // mbsbwfm
	echo "<br>";
	echo ubah_huruf('keren'); // lfsfo
	echo "<br>";
	echo ubah_huruf('semangat'); // tfnbohbu

	?>
</body>
</html>